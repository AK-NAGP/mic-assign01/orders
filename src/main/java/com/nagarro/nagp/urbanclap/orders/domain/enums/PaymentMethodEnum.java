package com.nagarro.nagp.urbanclap.orders.domain.enums;

public enum PaymentMethodEnum {
    CREDIT_CARD,
    NET_BANKING,
    WALLET,
    COD
}
