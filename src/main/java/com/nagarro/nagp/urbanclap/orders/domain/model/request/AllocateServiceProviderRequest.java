package com.nagarro.nagp.urbanclap.orders.domain.model.request;

import lombok.Data;

@Data
public class AllocateServiceProviderRequest {

    private String serviceProviderId;

    private String orderId;

    private Boolean acceptOrDeny;

}
