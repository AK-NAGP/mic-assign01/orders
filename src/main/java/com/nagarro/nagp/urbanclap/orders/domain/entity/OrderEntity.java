package com.nagarro.nagp.urbanclap.orders.domain.entity;

import com.nagarro.nagp.urbanclap.orders.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.orders.domain.enums.PaymentMethodEnum;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity(name = "orders")
@Data
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    private UUID paymentId;

    @Column(nullable = false)
    private String customerName;

    @Column(nullable = false)
    private Integer customerId;

    @Column(nullable = false)
    private Integer serviceId;

    @Column
    private Integer serviceProviderId;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private OrderStatusEnum orderStatus;

    @Column(nullable = false)
    private Double charges;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentMethodEnum paymentMethod;

    @Column
    private Date paymentOn;

    @Column
    private Date completedOn;

    @Column(nullable = false)
    private Date createdOn;

}
