package com.nagarro.nagp.urbanclap.orders.domain.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nagarro.nagp.urbanclap.orders.domain.model.UserDetailsModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AcceptOrderResponse extends BaseResponse {

    private UserDetailsModel userDetails;

    public AcceptOrderResponse(String status, String message) {
        super(status, message);
    }
}
