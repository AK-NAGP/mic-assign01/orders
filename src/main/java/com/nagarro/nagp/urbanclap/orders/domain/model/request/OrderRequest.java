package com.nagarro.nagp.urbanclap.orders.domain.model.request;

import com.nagarro.nagp.urbanclap.orders.domain.enums.PaymentMethodEnum;
import lombok.Data;

@Data
public class OrderRequest {

    private Integer serviceId;

    private Integer userId;

    private PaymentMethodEnum paymentMethod;
}
