package com.nagarro.nagp.urbanclap.orders.repository;

import com.nagarro.nagp.urbanclap.orders.domain.entity.OrderEntity;
import com.nagarro.nagp.urbanclap.orders.domain.enums.OrderStatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Integer> {

    Optional<OrderEntity> findByPaymentId(UUID paymentId);

    List<OrderEntity> findByCustomerIdAndOrderStatusNotIn(Integer customerId, List<OrderStatusEnum> orderStatus);

    List<OrderEntity> findByOrderStatusIn(List<OrderStatusEnum> orderStatus);

    List<OrderEntity> findAllByServiceProviderIdAndOrderStatus(Integer serviceProviderId, OrderStatusEnum orderStatus);
}
