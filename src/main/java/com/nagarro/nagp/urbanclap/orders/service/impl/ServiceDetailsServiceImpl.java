package com.nagarro.nagp.urbanclap.orders.service.impl;

import com.nagarro.nagp.urbanclap.orders.config.HystrixValuesConfig;
import com.nagarro.nagp.urbanclap.orders.domain.model.ServiceDetailsModel;
import com.nagarro.nagp.urbanclap.orders.domain.model.UserDetailsModel;
import com.nagarro.nagp.urbanclap.orders.service.ServiceDetailsService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.hystrix.HystrixCommands;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@Service
public class ServiceDetailsServiceImpl implements ServiceDetailsService {

    private final Map<String, ServiceDetailsModel> SERVICE_CACHE = new HashMap<>();

    @Autowired
    private WebClient.Builder webClientBuilder;

    @Autowired
    private HystrixValuesConfig hystrixConfig;

    @Override
    public ServiceDetailsModel getServiceDetails(String serviceId) {
        if (SERVICE_CACHE.containsKey(serviceId)) {
            return SERVICE_CACHE.get(serviceId);
        } else {
            ServiceDetailsModel serviceDetailsModel = getMonoServiceDetails(serviceId).block();
            assert serviceDetailsModel != null;
            SERVICE_CACHE.put(serviceId, serviceDetailsModel);
            return serviceDetailsModel;
        }
    }

    @Override
    public Mono<ServiceDetailsModel> getMonoServiceDetails(String serviceId) {
        Mono<ServiceDetailsModel> apiData = webClientBuilder
                .build()
                .get()
                .uri("http://UC-SERVICE-CATALOG/product-catalog/internal/services/" + serviceId)
                .retrieve()
                .bodyToMono(ServiceDetailsModel.class);

        return HystrixCommands
                .from(apiData)
                .commandProperties(setter -> {
                    setter.withExecutionTimeoutInMilliseconds(hystrixConfig.getExecutionTimeoutInMilliseconds());
                    setter.withCircuitBreakerEnabled(true);
                    setter.withRequestLogEnabled(true);
                    setter.withExecutionIsolationSemaphoreMaxConcurrentRequests(hystrixConfig.getRequestVolumeThreshold());
                    setter.withCircuitBreakerSleepWindowInMilliseconds(hystrixConfig.getSleepWindowInMilliseconds());
                    setter.withMetricsRollingStatisticalWindowInMilliseconds(hystrixConfig.getMetricsTimeInMillisecond());
                    return setter;
                })
                .fallback(Mono.just(getMonoServiceDetailsFallback(serviceId)))
                .commandName("getMonoServiceDetails")
                .toMono();
    }

    private ServiceDetailsModel getMonoServiceDetailsFallback(String serviceId) {
        if (SERVICE_CACHE.containsKey(serviceId)) {
            return SERVICE_CACHE.get(serviceId);
        } else {
            ServiceDetailsModel serviceDetailsModel = new ServiceDetailsModel();
            serviceDetailsModel.setIsServiceAvailable(false);

            return serviceDetailsModel;
        }
    }
}
