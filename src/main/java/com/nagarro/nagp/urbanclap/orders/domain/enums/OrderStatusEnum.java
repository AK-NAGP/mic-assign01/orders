package com.nagarro.nagp.urbanclap.orders.domain.enums;

public enum OrderStatusEnum {
    PAYMENT_IN_PROGRESS,
    PAYMENT_DONE,
    IN_PROGRESS,
    PROVIDER_APPROVAL_AWAIT,
    SERVICE_IN_PROGRESS,
    COMPLETED
}
