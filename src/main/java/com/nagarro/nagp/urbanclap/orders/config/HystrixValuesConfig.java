package com.nagarro.nagp.urbanclap.orders.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class HystrixValuesConfig {

    @Value("${hystrix.command.HystrixCommandKey.execution.isolation.thread.timeoutInMilliseconds}")
    private Integer executionTimeoutInMilliseconds;

    @Value("${hystrix.command.HystrixCommandKey.circuitBreaker.requestVolumeThreshold}")
    private Integer requestVolumeThreshold;

    @Value("${hystrix.command.HystrixCommandKey.circuitBreaker.sleepWindowInMilliseconds}")
    private Integer sleepWindowInMilliseconds;

    @Value("${hystrix.command.HystrixCommandKey.metrics.rollingStats.timeInMilliseconds}")
    private Integer metricsTimeInMillisecond;

}
