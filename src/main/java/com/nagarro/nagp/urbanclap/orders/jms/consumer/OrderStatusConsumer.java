package com.nagarro.nagp.urbanclap.orders.jms.consumer;

import com.google.gson.Gson;
import com.nagarro.nagp.urbanclap.orders.domain.model.jms.OrderStatusModel;
import com.nagarro.nagp.urbanclap.orders.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class OrderStatusConsumer {

    @Autowired
    private OrderService orderService;

    @Autowired
    private Gson gson;

    @JmsListener(destination = "uc.orders.updateOrderStatus")
    public void consumeOrderStatusUpdate(String message) {
        OrderStatusModel orderStatusModel = gson.fromJson(message, OrderStatusModel.class);
        orderService.updateOrderStatus(orderStatusModel.getOrderStatus(), Integer.parseInt(orderStatusModel.getOrderId()));
    }
}
