package com.nagarro.nagp.urbanclap.orders.service;

import com.nagarro.nagp.urbanclap.orders.domain.model.ServiceDetailsModel;
import reactor.core.publisher.Mono;

public interface ServiceDetailsService {

    ServiceDetailsModel getServiceDetails(String serviceId);

    Mono<ServiceDetailsModel> getMonoServiceDetails(String serviceId);
}
