package com.nagarro.nagp.urbanclap.orders.service.impl;

import com.nagarro.nagp.urbanclap.orders.config.HystrixValuesConfig;
import com.nagarro.nagp.urbanclap.orders.domain.model.UserDetailsModel;
import com.nagarro.nagp.urbanclap.orders.service.UserDetailsService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.hystrix.HystrixCommands;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final Map<String, UserDetailsModel> SERVICE_CACHE = new HashMap<>();

    @Autowired
    private WebClient.Builder webClientBuilder;

    @Autowired
    private HystrixValuesConfig hystrixConfig;

    @Override
    public UserDetailsModel getUserDetails(String userId) {
        if (SERVICE_CACHE.containsKey(userId)) {
            return SERVICE_CACHE.get(userId);
        } else {
            UserDetailsModel userDetailsModel = getUserMonoDetails(userId).block();
            assert userDetailsModel != null;
            SERVICE_CACHE.put(userId, userDetailsModel);
            return userDetailsModel;
        }
    }

    @Override
    public Mono<UserDetailsModel> getUserMonoDetails(String userId) {
        Mono<UserDetailsModel> apiData = webClientBuilder
                .build()
                .get()
                .uri("http://UC-USER-MANAGEMENT/user-management/internal/user/" + userId)
                .retrieve()
                .bodyToMono(UserDetailsModel.class);

        return HystrixCommands
                .from(apiData)
                .commandProperties(setter -> {
                    setter.withExecutionTimeoutInMilliseconds(hystrixConfig.getExecutionTimeoutInMilliseconds());
                    setter.withCircuitBreakerEnabled(true);
                    setter.withRequestLogEnabled(true);
                    setter.withExecutionIsolationSemaphoreMaxConcurrentRequests(hystrixConfig.getRequestVolumeThreshold());
                    setter.withCircuitBreakerSleepWindowInMilliseconds(hystrixConfig.getSleepWindowInMilliseconds());
                    setter.withMetricsRollingStatisticalWindowInMilliseconds(hystrixConfig.getMetricsTimeInMillisecond());
                    return setter;
                })
                .fallback(Mono.just(getUserMonoDetailsFallback(userId)))
                .commandName("getUserMonoDetails")
                .toMono();
    }

    private UserDetailsModel getUserMonoDetailsFallback(String userId) {
        if (SERVICE_CACHE.containsKey(userId)) {
            return SERVICE_CACHE.get(userId);
        } else {
            return new UserDetailsModel();
        }
    }
}
