package com.nagarro.nagp.urbanclap.orders.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ServiceDetailsModel {

    private Integer id;

    private String serviceName;

    private String serviceType;

    private Double charges;

    private Boolean isServiceAvailable;

}
