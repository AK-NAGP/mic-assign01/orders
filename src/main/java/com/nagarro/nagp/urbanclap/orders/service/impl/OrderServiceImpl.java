package com.nagarro.nagp.urbanclap.orders.service.impl;

import com.google.common.base.Splitter;
import com.nagarro.nagp.urbanclap.orders.domain.entity.OrderEntity;
import com.nagarro.nagp.urbanclap.orders.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.orders.domain.enums.PaymentMethodEnum;
import com.nagarro.nagp.urbanclap.orders.domain.model.ServiceDetailsModel;
import com.nagarro.nagp.urbanclap.orders.domain.model.ServiceProviderModel;
import com.nagarro.nagp.urbanclap.orders.domain.model.UserDetailsModel;
import com.nagarro.nagp.urbanclap.orders.domain.model.jms.OrderStatusModel;
import com.nagarro.nagp.urbanclap.orders.domain.model.jms.PaymentRequestModel;
import com.nagarro.nagp.urbanclap.orders.domain.model.request.AllocateServiceProviderRequest;
import com.nagarro.nagp.urbanclap.orders.domain.model.request.OrderRequest;
import com.nagarro.nagp.urbanclap.orders.domain.model.response.*;
import com.nagarro.nagp.urbanclap.orders.jms.producer.NotificationProducer;
import com.nagarro.nagp.urbanclap.orders.jms.producer.PaymentEntityProducer;
import com.nagarro.nagp.urbanclap.orders.jms.producer.ServiceProviderProducer;
import com.nagarro.nagp.urbanclap.orders.repository.OrderRepository;
import com.nagarro.nagp.urbanclap.orders.service.OrderService;
import com.nagarro.nagp.urbanclap.orders.service.ServiceDetailsService;
import com.nagarro.nagp.urbanclap.orders.service.UserDetailsService;
import com.nagarro.nagp.urbanclap.orders.service.UserServiceProviderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static com.nagarro.nagp.urbanclap.orders.constants.OrderConstants.MAX_CONCURRENT_ORDERS;
import static com.nagarro.nagp.urbanclap.orders.constants.OrderConstants.ORDER_STATUS_COMPLETED;
import static com.nagarro.nagp.urbanclap.orders.constants.OrderConstants.ORDER_STATUS_IN_PROGRESS;
import static com.nagarro.nagp.urbanclap.orders.constants.OrderConstants.ORDER_STATUS_ALLOCATE_SP;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private ServiceDetailsService serviceDetailsService;

    @Autowired
    private UserServiceProviderService serviceProviderService;

    @Autowired
    private ServiceProviderProducer serviceProviderProducer;

    @Autowired
    private NotificationProducer notificationProducer;

    @Autowired
    private PaymentEntityProducer paymentEntityProducer;

    @Override
    public OrderResponse createOrder(OrderRequest orderRequest) {
        OrderResponse orderResponse = new OrderResponse("success", "order created successfully");

        Mono<ServiceDetailsModel> serviceDetailsModelMono = serviceDetailsService.getMonoServiceDetails(orderRequest.getServiceId().toString());

        UserDetailsModel userDetailsModel = userDetailsService.getUserDetails(orderRequest.getUserId().toString());

        ServiceDetailsModel serviceDetailsModel = serviceDetailsModelMono.block();

        if (! serviceDetailsModel.getIsServiceAvailable()) {
            return new OrderResponse("failure", "This Service is currently not available");
        }

        List<OrderEntity> lastActiveOrders = orderRepository.findByCustomerIdAndOrderStatusNotIn(
                orderRequest.getUserId(),
                ORDER_STATUS_COMPLETED);

        if (lastActiveOrders.size() >= MAX_CONCURRENT_ORDERS) {
            return new OrderResponse("429", "Your last " + MAX_CONCURRENT_ORDERS + " orders are in progress");
        }

        UUID paymentId = UUID.randomUUID();

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setCharges(serviceDetailsModel.getCharges());
        orderEntity.setCustomerName(userDetailsModel.getName());
        orderEntity.setCustomerId(userDetailsModel.getId());
        orderEntity.setServiceId(serviceDetailsModel.getId());
        orderEntity.setPaymentId(paymentId);
        orderEntity.setOrderStatus(getOrderStatusOnPayment(orderRequest.getPaymentMethod()));
        orderEntity.setPaymentMethod(orderRequest.getPaymentMethod());
        orderEntity.setCreatedOn(new Date());

        orderRepository.save(orderEntity);

        //Send Payment Info to Payment Microservice...
        CompletableFuture.runAsync(() -> createPaymentInformation(orderEntity, serviceDetailsModel.getServiceName()));

        BeanUtils.copyProperties(orderEntity, orderResponse);
        orderResponse.setOrderId(orderEntity.getId());

        orderResponse.setUserDetails(userDetailsModel);
        orderResponse.setServiceDetails(serviceDetailsModel);

        //Change Response message if PAYMENT IS NOT COD
        if (! orderEntity.getPaymentMethod().equals(PaymentMethodEnum.COD)) {
            orderResponse.setMessage("Please Proceed to Payment for Order Finalization");
        }
        //Trigger Notification to the user for Thank You for Ordering/We are working on it...
        CompletableFuture.runAsync(() -> sendNotification(orderEntity));
        return orderResponse;
    }

    private void createPaymentInformation(OrderEntity orderEntity, String serviceName) {
        if (! orderEntity.getPaymentMethod().equals(PaymentMethodEnum.COD)) {
            PaymentRequestModel paymentRequestModel = new PaymentRequestModel();

            paymentRequestModel.setPaymentId(orderEntity.getPaymentId());
            paymentRequestModel.setOrderId(orderEntity.getId().toString());
            paymentRequestModel.setPaymentMode(orderEntity.getPaymentMethod().toString());
            paymentRequestModel.setCharges(orderEntity.getCharges());
            paymentRequestModel.setServiceName(serviceName);

            paymentEntityProducer.createPaymentEntity(paymentRequestModel);
        }
    }

    private OrderStatusEnum getOrderStatusOnPayment(PaymentMethodEnum paymentMethodEnum) {
        return paymentMethodEnum.equals(PaymentMethodEnum.COD) ?
                OrderStatusEnum.IN_PROGRESS
                : OrderStatusEnum.PAYMENT_IN_PROGRESS;
    }

    @Override
    public OrderResponse getOrderDetails(Integer id) {
        Optional<OrderEntity> orderEntityOptional = orderRepository.findById(id);
        if (orderEntityOptional.isPresent()) {
            OrderEntity orderEntity = orderEntityOptional.get();

            return mapEntityToResponse(orderEntity);
        } else {
            return new OrderResponse("failure", "Order not found");
        }
    }

    private OrderResponse mapEntityToResponse(OrderEntity orderEntity) {
        OrderResponse orderResponse = new OrderResponse();
        BeanUtils.copyProperties(orderEntity, orderResponse);
        orderResponse.setOrderId(orderEntity.getId());

        orderResponse.setServiceDetails(serviceDetailsService.getServiceDetails(orderEntity.getServiceId().toString()));
        orderResponse.setUserDetails(userDetailsService.getUserDetails(orderEntity.getCustomerId().toString()));

        if (Objects.nonNull(orderEntity.getServiceProviderId())) {
            orderResponse.setServiceProvider(serviceProviderService.getServiceProvider(orderEntity.getServiceProviderId().toString()));
        }

        return orderResponse;

    }

    @Override
    public OrderListResponse getAllOrdersByStatuses(String statuses) {
        List<OrderStatusEnum> orderStatusEnums = Splitter.on(',')
                .trimResults()
                .omitEmptyStrings()
                .splitToList(StringUtils.defaultIfBlank(statuses, ""))
                .stream()
                .map(OrderStatusEnum::valueOf)
                .collect(Collectors.toList());

        List<OrderEntity> entities;

        if (orderStatusEnums.isEmpty()) {
            entities = orderRepository.findByOrderStatusIn(ORDER_STATUS_IN_PROGRESS);
        } else {
            entities = orderRepository.findByOrderStatusIn(orderStatusEnums);
        }

        List<OrderResponse> orderResponses = entities
                .stream()
                .map(this::mapEntityToResponse)
                .collect(Collectors.toList());

        OrderListResponse orderListResponse = new OrderListResponse("success", "all current orders");
        orderListResponse.setOrders(orderResponses);

        return orderListResponse;
    }

    @Override
    public void updateOrderStatus(OrderStatusEnum orderStatusEnum, Integer orderId) {
        Optional<OrderEntity> optionalOrderEntity = orderRepository.findById(orderId);
        optionalOrderEntity.ifPresent(orderEntity -> updateOrderStatus(orderStatusEnum, orderEntity));
    }

    private void updateOrderStatus(OrderStatusEnum orderStatusEnum, OrderEntity orderEntity) {
        orderEntity.setOrderStatus(orderStatusEnum);

        if (! orderStatusEnum.equals(OrderStatusEnum.IN_PROGRESS)) {
            //Send Notification...
            CompletableFuture.runAsync(() -> sendNotification(orderEntity));
        }

        if (orderStatusEnum.equals(OrderStatusEnum.IN_PROGRESS)) {
            orderEntity.setServiceProviderId(null);
        }

        if (orderStatusEnum.equals(OrderStatusEnum.PAYMENT_DONE) && ! orderEntity.getPaymentMethod().equals(PaymentMethodEnum.COD)) {
            orderEntity.setOrderStatus(OrderStatusEnum.IN_PROGRESS);
        }


        if (orderStatusEnum.equals(OrderStatusEnum.PAYMENT_DONE) ||
                //Update it for COD orders also
                (
                        orderEntity.getOrderStatus().equals(OrderStatusEnum.COMPLETED) &&
                                orderEntity.getPaymentMethod().equals(PaymentMethodEnum.COD)
                )
        ) {
            orderEntity.setPaymentOn(new Date());
        }

        if (orderStatusEnum.equals(OrderStatusEnum.COMPLETED)) {
            orderEntity.setCreatedOn(new Date());
        }


        orderRepository.save(orderEntity);
    }

    private void sendNotification(OrderEntity orderEntity) {
        OrderResponse orderResponse = mapEntityToResponse(orderEntity);
        notificationProducer.sendNotification(orderResponse);
    }

    @Override
    public AllocateSponsorResponse allocateServiceProvider(AllocateServiceProviderRequest allocateServiceProviderRequest) {

        Optional<OrderEntity> orderEntityOptional = orderRepository.findById(Integer.parseInt(allocateServiceProviderRequest.getOrderId()));

        if (orderEntityOptional.isPresent()) {
            OrderEntity orderEntity = orderEntityOptional.get();
            if (ORDER_STATUS_ALLOCATE_SP.contains(orderEntity.getOrderStatus())) {
                AllocateSponsorResponse allocateSponsorResponse = new AllocateSponsorResponse();

                ServiceProviderModel serviceProviderModel = serviceProviderService
                        .getServiceProviderDetailsMono(allocateServiceProviderRequest.getServiceProviderId()).block();

                if (serviceProviderModel.getIsAvailable()) {
                    orderEntity.setServiceProviderId(serviceProviderModel.getId());

                    //Update Entity in DB
                    CompletableFuture.runAsync(() -> updateOrderStatus(OrderStatusEnum.PROVIDER_APPROVAL_AWAIT, orderEntity));

                    BeanUtils.copyProperties(orderEntity, allocateSponsorResponse);
                    allocateSponsorResponse.setServiceProvider(serviceProviderModel);

                    return allocateSponsorResponse;
                } else {
                    return new AllocateSponsorResponse("failure", "Service Provider not available");
                }
            } else {
                return new AllocateSponsorResponse("failure", "Order not in correct status for this Allocation");
            }
        } else {
            return new AllocateSponsorResponse("failure", "Order not found");
        }
    }

    @Override
    public AcceptOrderResponse acceptOrderResponse(AllocateServiceProviderRequest acceptOrderModel) {
        Optional<OrderEntity> orderEntityOptional = orderRepository.findById(Integer.parseInt(acceptOrderModel.getOrderId()));
        if (orderEntityOptional.isPresent()) {
            OrderEntity orderEntity = orderEntityOptional.get();

            //Check for Auth Person is changing the status...
            String serviceProviderId = Objects.nonNull(orderEntity.getServiceProviderId()) ? orderEntity.getServiceProviderId().toString() : "";
            if (serviceProviderId.equals(acceptOrderModel.getServiceProviderId())) {

                //Check for the ORDER IS IN CORRECT STATUS...
                if (orderEntity.getOrderStatus().equals(OrderStatusEnum.PROVIDER_APPROVAL_AWAIT)) {

                    //Check for acceptance or denial
                    if (acceptOrderModel.getAcceptOrDeny()) {
                        updateOrderStatus(OrderStatusEnum.SERVICE_IN_PROGRESS, orderEntity);
                        AcceptOrderResponse acceptOrderResponse = new AcceptOrderResponse("success", "Order Successfully Accepted");
                        acceptOrderResponse.setUserDetails(userDetailsService.getUserDetails(orderEntity.getCustomerId().toString()));

                        //Send Unavailable status to Queue. So no new orders will not be assigned to this Service Provider
                        serviceProviderProducer.sendServiceProviderAvailability(new ServiceProviderModel(orderEntity.getServiceProviderId(), false));

                        return acceptOrderResponse;
                    } else {
                        updateOrderStatus(OrderStatusEnum.IN_PROGRESS, orderEntity);
                        return new AcceptOrderResponse("success", "Order Successfully Denied");
                    }

                } else {
                    return new AcceptOrderResponse("failure", "Order not in correct status for Acceptance");
                }
            } else {
                return new AcceptOrderResponse("Unauthorized", "Order does not belongs to you");
            }
        } else {
            return new AcceptOrderResponse("failure", "Order not found");
        }
    }

    @Override
    public BaseResponse completeOrder(String orderId) {
        Optional<OrderEntity> orderEntityOptional = orderRepository.findById(Integer.parseInt(orderId));
        if (orderEntityOptional.isPresent()) {
            OrderEntity orderEntity = orderEntityOptional.get();

            if (orderEntity.getOrderStatus().equals(OrderStatusEnum.SERVICE_IN_PROGRESS)) {

                if (orderEntity.getPaymentMethod().equals(PaymentMethodEnum.COD)) {
                    updateOrderStatus(OrderStatusEnum.PAYMENT_DONE, orderEntity);
                }

                CompletableFuture.runAsync(() -> updateOrderStatus(OrderStatusEnum.COMPLETED, orderEntity));

                //Send Available status to Queue. So new orders will be assigned to this Service Provider
                serviceProviderProducer.sendServiceProviderAvailability(new ServiceProviderModel(orderEntity.getServiceProviderId(), true));

                return new BaseResponse("success", "Order Completed");
            } else {
                return new AcceptOrderResponse("failure", "Order not in correct status for Complete");
            }
        } else {
            return new BaseResponse("failure", "Order not found");
        }
    }

    @Override
    public OrderListResponse getOrdersByServiceProviderId(AllocateServiceProviderRequest allocateServiceProviderRequest) {

        List<OrderEntity> entities = orderRepository.findAllByServiceProviderIdAndOrderStatus(
                Integer.parseInt(allocateServiceProviderRequest.getServiceProviderId()),
                OrderStatusEnum.PROVIDER_APPROVAL_AWAIT
        );

        List<OrderResponse> orderResponses = entities
                .stream()
                .map(this::mapEntityToResponse)
                .collect(Collectors.toList());

        OrderListResponse orderListResponse = new OrderListResponse("success", "all my orders");
        orderListResponse.setOrders(orderResponses);

        return orderListResponse;
    }


}
