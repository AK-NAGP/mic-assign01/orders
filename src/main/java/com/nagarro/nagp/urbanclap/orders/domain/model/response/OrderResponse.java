package com.nagarro.nagp.urbanclap.orders.domain.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nagarro.nagp.urbanclap.orders.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.orders.domain.enums.PaymentMethodEnum;
import com.nagarro.nagp.urbanclap.orders.domain.model.ServiceDetailsModel;
import com.nagarro.nagp.urbanclap.orders.domain.model.ServiceProviderModel;
import com.nagarro.nagp.urbanclap.orders.domain.model.UserDetailsModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderResponse extends BaseResponse {

    private Integer orderId;

    private Double charges;

    private OrderStatusEnum orderStatus;

    private PaymentMethodEnum paymentMethod;

    private UUID paymentId;

    private Date createdOn;

    private Date completedOn;

    private Date paymentOn;

    private ServiceDetailsModel serviceDetails;

    private ServiceProviderModel serviceProvider;

    private UserDetailsModel userDetails;

    public OrderResponse(String status, String message) {
        super(status, message);
    }
}
