package com.nagarro.nagp.urbanclap.orders.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ServiceProviderModel {

    private Integer id;

    private String name;

    private String username;

    private String email;

    private String phone;

//    private List<String> servicablePincodes;

    private Boolean isAvailable;

    public ServiceProviderModel(Integer id, Boolean isAvailable) {
        this.id = id;
        this.isAvailable = isAvailable;
    }

}
