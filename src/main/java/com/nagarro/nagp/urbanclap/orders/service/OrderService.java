package com.nagarro.nagp.urbanclap.orders.service;

import com.nagarro.nagp.urbanclap.orders.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.orders.domain.model.request.AllocateServiceProviderRequest;
import com.nagarro.nagp.urbanclap.orders.domain.model.request.OrderRequest;
import com.nagarro.nagp.urbanclap.orders.domain.model.response.*;

public interface OrderService {

    OrderResponse createOrder(OrderRequest orderRequest);

    OrderResponse getOrderDetails(Integer id);

    OrderListResponse getAllOrdersByStatuses(String statuses);

    void updateOrderStatus(OrderStatusEnum orderStatusEnum, Integer orderId);

    AllocateSponsorResponse allocateServiceProvider(AllocateServiceProviderRequest allocateServiceProviderRequest);


    AcceptOrderResponse acceptOrderResponse(AllocateServiceProviderRequest acceptOrderModel);

    BaseResponse completeOrder(String orderId);

    OrderListResponse getOrdersByServiceProviderId(AllocateServiceProviderRequest allocateServiceProviderRequest);
}
