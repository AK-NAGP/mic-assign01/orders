package com.nagarro.nagp.urbanclap.orders.jms.producer;

import com.google.gson.Gson;
import com.nagarro.nagp.urbanclap.orders.domain.model.response.OrderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class NotificationProducer {

    private static final String JMS_NOTIFICATION_Q = "uc.notification.sendNotification";

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Gson gson;

    public void sendNotification(OrderResponse orderResponse) {
        String jsonData = gson.toJson(orderResponse);
        jmsTemplate.convertAndSend(JMS_NOTIFICATION_Q, jsonData);
    }

}
