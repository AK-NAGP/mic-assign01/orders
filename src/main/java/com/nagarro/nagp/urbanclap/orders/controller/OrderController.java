package com.nagarro.nagp.urbanclap.orders.controller;

import com.nagarro.nagp.urbanclap.orders.domain.model.request.AllocateServiceProviderRequest;
import com.nagarro.nagp.urbanclap.orders.domain.model.request.OrderRequest;
import com.nagarro.nagp.urbanclap.orders.domain.model.response.*;
import com.nagarro.nagp.urbanclap.orders.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("create")
    public ResponseEntity<OrderResponse> createOrder(
            @RequestBody OrderRequest orderRequest
    ) {
        return ResponseEntity.ok(orderService.createOrder(orderRequest));
    }

    @GetMapping("all")
    public ResponseEntity<OrderListResponse> getAllOrders(
            @RequestParam(name = "statuses", required = false) String statuses
    ) {
        return ResponseEntity.ok(orderService.getAllOrdersByStatuses(statuses));
    }

    @GetMapping("{id}")
    public ResponseEntity<OrderResponse> getOrderById(
            @PathVariable("id") Integer id
    ) {
        return ResponseEntity.ok(orderService.getOrderDetails(id));
    }

    @PostMapping("my-orders")
    public ResponseEntity<OrderListResponse> getAllAllocationsByServiceProviderId(
            @RequestBody AllocateServiceProviderRequest allocateServiceProviderRequest
    ) {
        return ResponseEntity.ok(orderService.getOrdersByServiceProviderId(allocateServiceProviderRequest));
    }

    @PostMapping("allocate")
    public ResponseEntity<AllocateSponsorResponse> allocateServiceProvider(
            @RequestBody AllocateServiceProviderRequest allocateServiceProviderRequest
    ) {
        return ResponseEntity.ok(orderService.allocateServiceProvider(allocateServiceProviderRequest));
    }

    @PostMapping("accept")
    public ResponseEntity<AcceptOrderResponse> acceptOrder(
            @RequestBody AllocateServiceProviderRequest allocateServiceProviderRequest
    ) {
        return ResponseEntity.ok(orderService.acceptOrderResponse(allocateServiceProviderRequest));
    }

    @GetMapping("complete/{orderId}")
    public ResponseEntity<BaseResponse> acceptOrder(
            @PathVariable("orderId") String orderId
    ) {
        return ResponseEntity.ok(orderService.completeOrder(orderId));
    }
}
