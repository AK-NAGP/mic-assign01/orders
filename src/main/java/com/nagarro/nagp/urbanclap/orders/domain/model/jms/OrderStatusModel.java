package com.nagarro.nagp.urbanclap.orders.domain.model.jms;

import com.nagarro.nagp.urbanclap.orders.domain.enums.OrderStatusEnum;
import lombok.Data;

@Data
public class OrderStatusModel {

    private String orderId;

    private OrderStatusEnum orderStatus;

}
