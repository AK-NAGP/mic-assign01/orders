package com.nagarro.nagp.urbanclap.orders.domain.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nagarro.nagp.urbanclap.orders.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.orders.domain.enums.PaymentMethodEnum;
import com.nagarro.nagp.urbanclap.orders.domain.model.ServiceProviderModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AllocateSponsorResponse extends BaseResponse {

    private Integer orderId;

    private Double charges;

    private OrderStatusEnum orderStatus;

    private PaymentMethodEnum paymentMethod;

    private UUID paymentId;

    private ServiceProviderModel serviceProvider;

    public AllocateSponsorResponse(String status, String message) {
        super(status, message);
    }
}
