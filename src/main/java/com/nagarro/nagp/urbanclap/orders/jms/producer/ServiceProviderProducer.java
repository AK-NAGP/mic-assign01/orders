package com.nagarro.nagp.urbanclap.orders.jms.producer;

import com.google.gson.Gson;
import com.nagarro.nagp.urbanclap.orders.domain.model.ServiceProviderModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class ServiceProviderProducer {

    private static final String JMS_SERVICE_PROVIDER_STATUS_Q = "uc.ums.serviceProviderStatus";

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Gson gson;

    public void sendServiceProviderAvailability(ServiceProviderModel serviceProviderModel) {
        String jsonData = gson.toJson(serviceProviderModel);
        jmsTemplate.convertAndSend(JMS_SERVICE_PROVIDER_STATUS_Q, jsonData);
    }
}
