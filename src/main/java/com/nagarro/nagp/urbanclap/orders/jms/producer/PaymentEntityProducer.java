package com.nagarro.nagp.urbanclap.orders.jms.producer;

import com.google.gson.Gson;
import com.nagarro.nagp.urbanclap.orders.domain.model.jms.PaymentRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class PaymentEntityProducer {

    private static final String JMS_PAYMENT_Q = "uc.payments.addPaymentEntity";

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Gson gson;

    public void createPaymentEntity(PaymentRequestModel paymentRequestModel){
        String jsonData = gson.toJson(paymentRequestModel);
        jmsTemplate.convertAndSend(JMS_PAYMENT_Q, jsonData);
    }
}
