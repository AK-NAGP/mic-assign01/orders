package com.nagarro.nagp.urbanclap.orders.domain.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderListResponse extends BaseResponse {

    private List<OrderResponse> orders;

    public OrderListResponse(String status, String message) {
        super(status, message);
    }
}
