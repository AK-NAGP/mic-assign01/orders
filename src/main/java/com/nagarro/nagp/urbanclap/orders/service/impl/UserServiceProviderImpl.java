package com.nagarro.nagp.urbanclap.orders.service.impl;

import com.nagarro.nagp.urbanclap.orders.config.HystrixValuesConfig;
import com.nagarro.nagp.urbanclap.orders.domain.model.ServiceProviderModel;
import com.nagarro.nagp.urbanclap.orders.service.UserServiceProviderService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.hystrix.HystrixCommands;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceProviderImpl implements UserServiceProviderService {

    private final Map<String, ServiceProviderModel> SERVICE_CACHE = new HashMap<>();

    @Autowired
    private WebClient.Builder webClientBuilder;

    @Autowired
    private HystrixValuesConfig hystrixConfig;

    @Override
    public ServiceProviderModel getServiceProvider(String serviceProviderId) {
        if (SERVICE_CACHE.containsKey(serviceProviderId)) {
            return SERVICE_CACHE.get(serviceProviderId);
        } else {
            ServiceProviderModel serviceProviderModel = getServiceProviderDetailsMono(serviceProviderId).block();
            assert serviceProviderModel != null;
            SERVICE_CACHE.put(serviceProviderId, serviceProviderModel);
            return serviceProviderModel;
        }
    }

    @Override
    public Mono<ServiceProviderModel> getServiceProviderDetailsMono(String serviceProviderId) {
        Mono<ServiceProviderModel> apiData = webClientBuilder
                .build()
                .get()
                .uri("http://UC-USER-MANAGEMENT/user-management/internal/service-provider/" + serviceProviderId)
                .retrieve()
                .bodyToMono(ServiceProviderModel.class);

        return HystrixCommands
                .from(apiData)
                .commandProperties(setter -> {
                    setter.withExecutionTimeoutInMilliseconds(hystrixConfig.getExecutionTimeoutInMilliseconds());
                    setter.withCircuitBreakerEnabled(true);
                    setter.withRequestLogEnabled(true);
                    setter.withExecutionIsolationSemaphoreMaxConcurrentRequests(hystrixConfig.getRequestVolumeThreshold());
                    setter.withCircuitBreakerSleepWindowInMilliseconds(hystrixConfig.getSleepWindowInMilliseconds());
                    setter.withMetricsRollingStatisticalWindowInMilliseconds(hystrixConfig.getMetricsTimeInMillisecond());
                    return setter;
                })
                .fallback(Mono.just(getServiceProviderDetailsMonoFallback(serviceProviderId)))
                .commandName("getServiceProviderDetailsMono")
                .toMono();
    }

    private ServiceProviderModel getServiceProviderDetailsMonoFallback(String serviceProviderId) {
        if (SERVICE_CACHE.containsKey(serviceProviderId)) {
            return SERVICE_CACHE.get(serviceProviderId);
        } else {
            ServiceProviderModel serviceProviderModel = new ServiceProviderModel();
            serviceProviderModel.setIsAvailable(false);
            return serviceProviderModel;
        }
    }
}
