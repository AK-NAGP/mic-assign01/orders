package com.nagarro.nagp.urbanclap.orders.constants;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.nagarro.nagp.urbanclap.orders.domain.enums.OrderStatusEnum;

import java.util.List;
import java.util.Set;

public interface OrderConstants {

    List<OrderStatusEnum> ORDER_STATUS_COMPLETED = ImmutableList.of(OrderStatusEnum.COMPLETED);
    List<OrderStatusEnum> ORDER_STATUS_IN_PROGRESS = ImmutableList.of(OrderStatusEnum.IN_PROGRESS);
    Set<OrderStatusEnum> ORDER_STATUS_ALLOCATE_SP = ImmutableSet.of(OrderStatusEnum.IN_PROGRESS, OrderStatusEnum.PROVIDER_APPROVAL_AWAIT);

    Integer MAX_CONCURRENT_ORDERS = 2;
}
