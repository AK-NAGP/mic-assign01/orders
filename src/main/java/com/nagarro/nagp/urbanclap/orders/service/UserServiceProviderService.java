package com.nagarro.nagp.urbanclap.orders.service;

import com.nagarro.nagp.urbanclap.orders.domain.model.ServiceProviderModel;
import reactor.core.publisher.Mono;

public interface UserServiceProviderService {

    ServiceProviderModel getServiceProvider(String serviceProviderId);

    Mono<ServiceProviderModel> getServiceProviderDetailsMono(String serviceProviderId);

}
