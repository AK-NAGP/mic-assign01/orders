package com.nagarro.nagp.urbanclap.orders.domain.model.jms;

import lombok.Data;

import java.util.UUID;

@Data
public class PaymentRequestModel {
    private UUID paymentId;

    private String paymentMode;

    private String orderId;

    private Double charges;

    private String serviceName;
}
