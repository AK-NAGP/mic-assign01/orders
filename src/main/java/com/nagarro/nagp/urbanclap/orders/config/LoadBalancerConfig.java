package com.nagarro.nagp.urbanclap.orders.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.client.loadbalancer.reactive.LoadBalancerExchangeFilterFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class LoadBalancerConfig {

//    @Bean
//    WebClient webClient(LoadBalancerClient loadBalancerClient) {
//        return WebClient.builder()
//                .filter(new LoadBalancerExchangeFilterFunction(loadBalancerClient))
//                .build();
//    }

    @Bean
    @LoadBalanced
    public WebClient.Builder getWebClient(){
        return WebClient.builder();
    }
}
