package com.nagarro.nagp.urbanclap.orders.service;

import com.nagarro.nagp.urbanclap.orders.domain.model.UserDetailsModel;
import reactor.core.publisher.Mono;

public interface UserDetailsService {

    UserDetailsModel getUserDetails(String userId);

    Mono<UserDetailsModel> getUserMonoDetails(String userId);

}
